<?php

/* image upload */
$page_gen_start = time();
// require("./functions.php");
$session_id = time() * rand(1111, 9999);
$session_hash = md5($session_id);

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
            <style type="text/css">
            <!--
                a, a:active, a:hover, a:link, a:visited {
                    color: #ffffff;
                    text-decoration: none;
                }
                html, body {
                    background-color: #000000;
                    color: #ffffff;
                    font-family: "Courier New", Courier, monospace;
                    font-size: 1.5em;
                    height: 100%;
                    margin: 0;
                    padding: 0;
                }
                img {
                    border: 0px;
                }
                .inner {
                    left: 50%;
                    position: absolute;
                    text-align: left;
                    transform: translate(-50%, 0%);
                    /* top: 50%; */
                }
                .outer {
                    height: 100%;
                    position: relative;
                    width: 100%;
                }
                #form {
                    /* width: 500px; */
                    margin: 10px auto;
                    background: #222;
                    padding: 10px;
                    overflow: hidden;
                    -moz-border-radius: 20px;
                    -webkit-border-radius: 20px;
                    border-radius: 20px;
                }
                label, input, textarea { /* Stili comuni agli elementi del form */
                    color: #dedede; /* Colore del testo */
                    float: left; /* Float a sinistra */
                    font-family: "Courier New", Courier, monospace;
                    margin: 10px 0; /* Margini */
                }
                label { /* Stili per la label */
                    display: block; /* Impostiamo la label come elemento blocco */
                    line-height: 30px; /* Altezza di riga */
                    width: 500px;
                }
                input, textarea { /* Stili per il campo di testo e per la textarea */
                    background: #1C1C1C; /* Colore di sfondo */
                    border: 1px solid #323232; /* Bordo */
                    color: #fff; /* Colore del testo */
                    height: 30px; /* Altezza */
                    line-height: 30px; /* Altezza di riga */
                    width: 500px; /* Larghezza */
                    padding: 0 10px; /* Padding */
                }
                /* input { padding-left: 30px; } */
            -->
            </style>
    </head>

    <body>
        <div class="outer">
            <div class="inner">

<?php

$act = $_GET['act'];
$line1 = $_POST['color-line-1'];
$line2 = $_POST['color-line-2'];
$line3 = $_POST['color-line-3'];

if ($act == 'upload')
{
    $filename = $_FILES["bitmap"]["name"];
    $target_dir = "uploads/";
    $target_file = $target_dir . basename($_FILES["bitmap"]["name"]);
    move_uploaded_file($_FILES["bitmap"]["tmp_name"], $target_file);

    echo "[ / <a href=\"./load.php\">Home</a> / File Uploaded ] <br />\n";
    echo "+--------------------------------------------------+ <br />\n";
    echo "|.File uploaded....................................| <br />\n";
    echo "+--------------------------------------------------+ <br />\n";

    echo "<label>".$filename."</label><img width=\"500\" src=\"".$target_file."\" />\n";
    echo "<form action=\"json.php\" method=\"post\" enctype=\"multipart/form-data\">\n";
    echo "<label>JSON for line_1</label><input type=\"submit\" id=\"submit\" name=\"json\" value=\"[ extract $line1 ]\" />\n";
    echo "<input type=\"hidden\" name=\"line1\" value=\"$line1\" />\n";
    echo "</form>\n";
}
else
{
    echo "[ / <a href=\"./load.php\">Home</a> ] <br />\n";
    echo "+--------------------------------------------------+ <br />\n";
    echo "|.Upload your graph in bitmap format...............| <br />\n";
    echo "|.What a minute....................................| <br />\n";
    echo "|.Download your data in JSON.......................| <br />\n";
    echo "+--------------------------------------------------+ <br />\n";

    echo "<form action=\"./load.php?act=upload\" method=\"POST\" enctype=\"multipart/form-data\">\n";
    echo "<label>select a bitmap</label><input type=\"file\" name=\"bitmap\" id=\"bitmap\" style=\"width: 500px solid white;\" /> <br />\n";
    echo "<label>line_1</label><input type=\"text\" name=\"color-line-1\" value=\"RGB#000000\" /> <br />\n";
    echo "<label>line_2</label><input type=\"text\" name=\"color-line-2\" value=\"RGB#000000\" /> <br />\n";
    echo "<label>line_3</label><input type=\"text\" name=\"color-line-3\" value=\"RGB#000000\" /> <br />\n";
    echo "<label></label><input type=\"submit\" id=\"submit\" name=\"submit\" value=\"UPLOAD\" />\n";
    echo "</form>\n";
}

?>

            </div>
        </div>
    </body>
</html>

<?php

$page_gen_end = time();

?>
